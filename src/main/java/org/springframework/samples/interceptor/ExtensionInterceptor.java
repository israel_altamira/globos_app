package org.springframework.samples.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ExtensionInterceptor extends HandlerInterceptorAdapter {

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        long starttime = (Long) request.getAttribute("starttime");
        request.removeAttribute("starttime");
        long endtime = System.currentTimeMillis();
        modelAndView.addObject("handlingtime_extension", ((endtime - starttime) * 10000));
        System.out.println("ext handling time " + modelAndView.getModel());
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        long starttime = System.currentTimeMillis();
        request.setAttribute("starttime", starttime);
        System.out.println("ext start time: " + request.getAttribute("starttime"));
        return true;
    }

}
