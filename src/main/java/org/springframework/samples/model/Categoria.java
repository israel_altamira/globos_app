package org.springframework.samples.model;

//@Table {schema="test" table="categoria"}
public class Categoria {

    private String name = "";
    private String path = "/img";

    public Categoria(String nombre, String path) {
        this.setName(nombre);
        this.setPath(path);
    }

    // @column{name="NOMBRE"}
    public String getName() {
        return this.name;
    }

    // @column{name="PATH"}
    public String getPath() {
        return this.path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
