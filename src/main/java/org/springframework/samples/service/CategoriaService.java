package org.springframework.samples.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.samples.model.Categoria;

public class CategoriaService {

    public List<Categoria> getCategorias() {
        List<Categoria> list = new ArrayList<Categoria>();
        Categoria c1 = new Categoria("globos metalicos", "/img/metalico.png");
        Categoria c2 = new Categoria("globos lisos", "/img/liso.png");
        Categoria c3 = new Categoria("globos licencia", "/img/licencia.png");
        list.add(c1);
        list.add(c2);
        list.add(c3);
        return list;
    }
}
