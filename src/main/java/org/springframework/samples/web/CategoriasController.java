package org.springframework.samples.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.samples.model.Categoria;
import org.springframework.samples.service.CategoriaService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/categorias")
public class CategoriasController {

    private CategoriaService sampleService = null;

    @RequestMapping(method = RequestMethod.GET)
    public void handler(Model model) {
        List<Categoria> list = this.sampleService.getCategorias();
        model.addAttribute("categorias", list);
    }

    @Autowired
    public void setSampleService(CategoriaService sampleService) {
        this.sampleService = sampleService;
    }

}
