<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta charset="utf-8">
		<title>Welcome</title>
	</head> 
	<body>
		<h3>Por favor escoge una categoria</h3>
	</body>
	
	<c:forEach items="${categorias}" var="categoria">
		<a href=""><img height="60px" width="100px" src="${categoria.path}">${categoria.name} </img></a>
	</c:forEach>
	
	<h5>time to serve this request: ${handlingtime*10000}ms</h5>
	<h5>time to serve this request: ${handlingtime_extension}ms</h5>
</html>
